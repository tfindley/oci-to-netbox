# Oracle Cloud Infrastructure > NetBox

## CMDB - NetBox Support

If you wish to use the included NetBox integration with this, you will need to add the following Environmental Variables

|                 **Variable** 	| **Description**                         	|
|-----------------------------:	|-----------------------------------------	|
|           TF_VAR_NETBOX_URL 	| NetBox URL                              	|
|     TF_VAR_NETBOX_API_RW_KEY 	| NetBox API Key (with RW Access)         	|

I'll add optional netbox blocks in future releases