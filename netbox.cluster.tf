resource "netbox_cluster_group" "oci_region" {
  name = var.region
}

resource "netbox_cluster_type" "oci_compute" {
  name = "OCI Compute"
  slug = "oci_compute"
}

resource "netbox_cluster" "oci_availability_domain" {
  name             = var.availability_domain # oci_core_instance.tf_compute.availability_domain
  cluster_type_id  = netbox_cluster_type.oci_compute.id
  cluster_group_id = netbox_cluster_group.oci_region.id
  site_id          = netbox_site.oci_site.id
  tenant_id        = data.netbox_tenant.find_tenancy.id
}