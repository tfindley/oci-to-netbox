# ------------------------
# Environmental variables
# ------------------------
# These must be set in your environment and must be prefixed with TF_VAR_
# e.g: NETBOX_URL == TF_VAR_NETBOX_URL

# NetBox Variables

variable "NETBOX_URL" {
  description = "Netbox URL: Defined by Environment varaible"
  type        = string
}

variable "NETBOX_API_RW_KEY" {
  description = "Netbox API Read/Write Key: Defined by Environment varaible"
  type        = string
}


variable "netbox_tenancy" {
  type = string
}

variable "availability_domain" { # oci_core_instance.tf_compute.availability_domain
  type = string
}

variable "region" { # OCI Availability Domain
  type    = string
  default = "us-phoenix-1"
}

variable "region_description" {
  type    = string
  default = "OCI US Phoenix 1"
}

variable "compute_name" { # oci_core_instance.tf_compute.display_name
  type = string
}

# variable "compute_dns" { 
#   type = string
# }

variable "compute_disk_size_gb" { # oci_core_instance.tf_compute.source_details[0].boot_volume_size_in_gbs
  type = number
}

variable "compute_mem_size_gb" { # oci_core_instance.tf_compute.shape_config[0].memory_in_gbs
  type = number
}

variable "compute_ocpus" { # var.oci_core_instance.tf_compute.shape_config[0].ocpus
  type = number
}

variable "compute_nic_name" { # oci_core_instance.tf_compute.create_vnic_details[0].display_name
  type = string
}

variable "compute_nic_ipv4" { # oci_core_instance.tf_compute.create_vnic_details[0].private_ip
  type = string
}

variable "compute_nic_external" { # oci_core_instance.tf_compute.public_ip
  type = string
}

variable "network_subnet_prefix" { # oci_core_subnet.pubdev.cidr_block
  type = string
}

variable "oci_site_name" {
  type    = string
  default = "tfindley"
}

variable "timezone" {
  type    = string
  default = "UTC"
}

variable "facility" {
  type    = string
  default = "OCI Data center"
}


# ------------------------
# Lookups
# ------------------------
# Variables that we're best retrieving from OCI itself instead of statically defining them

# ------------------------

data "netbox_tenant" "find_tenancy" {
  name = var.netbox_tenancy
}