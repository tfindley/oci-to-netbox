resource "netbox_virtual_machine" "new_compute" {
  cluster_id   = resource.netbox_cluster.oci_availability_domain.id
  name         = var.compute_name         # oci_core_instance.tf_compute.display_name
  disk_size_gb = var.compute_disk_size_gb # oci_core_instance.tf_compute.source_details[0].boot_volume_size_in_gbs
  memory_mb    = var.compute_mem_size_gb  # oci_core_instance.tf_compute.shape_config[0].memory_in_gbs
  vcpus        = var.compute_ocpus        # var.oci_core_instance.tf_compute.shape_config[0].ocpus
  # role_id           = 13    # Not currently able to look up roles dynamically
  tenant_id = data.netbox_tenant.find_tenancy.id
  site_id   = resource.netbox_site.oci_site.id
}

resource "netbox_interface" "create_if" {
  name = var.compute_nic_name # oci_core_instance.tf_compute.create_vnic_details[0].display_name
  # mac_address         = resource.proxmox_lxc.basic.network[0].hwaddr
  virtual_machine_id = resource.netbox_virtual_machine.new_compute.id
}

# resource "netbox_ip_address" "assign_ip" {
#   ip_address = var.compute_nic_ipv4
#   tenant_id  = data.netbox_tenant.find_tenancy.id
#   # vrf_id            = resource.netbox_available_ip_address.reserve_ip.vrf_id
#   interface_id = resource.netbox_interface.create_if.id
#   # dns_name     = var.compute_dns
#   status = "active"
# }