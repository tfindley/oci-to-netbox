resource "netbox_site_group" "oci" {
  name        = "OCI"
  description = "Oracle Cloud Infrastructure"
}

resource "netbox_region" "oci" {
  name        = "OCI"
  description = "Oracle Cloud Infrastructure"
}

resource "netbox_region" "oci_region" {
  name             = var.region
  description      = var.region_description
  parent_region_id = netbox_region.oci.id
}

resource "netbox_site" "oci_site" {
  name      = var.oci_site_name # "tfindley"
  group_id  = resource.netbox_site_group.oci.id
  region_id = resource.netbox_region.oci_region.id
  facility  = var.facility
  status    = "active"
  timezone  = var.timezone
  tenant_id = data.netbox_tenant.find_tenancy.id
}