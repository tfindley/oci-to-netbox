resource "netbox_prefix" "oci_subnet" {
  prefix    = var.network_subnet_prefix # oci_core_subnet.pubdev.cidr_block
  status    = "active"
  tenant_id = data.netbox_tenant.find_tenancy.id
  site_id   = netbox_site.oci_site.id
}