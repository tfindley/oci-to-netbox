terraform {
  required_providers {
    netbox = {
      source = "e-breuninger/netbox"
      # version = "1.2.2"
    }
  }
}

provider "netbox" {
  server_url           = var.NETBOX_URL
  api_token            = var.NETBOX_API_RW_KEY
  allow_insecure_https = false
}
